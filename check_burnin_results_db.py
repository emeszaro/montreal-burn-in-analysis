import numpy as np
import pandas as pd
import seaborn as sns
import sys
import json
import random as r
from tqdm import tqdm 
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap, BoundaryNorm
from matplotlib.cm import ScalarMappable
from databaseUtilitiesEthan import commands as dbCommands

outdir = sys.argv[1]
save_name = 'hybrid_test_results_db.png'
print(f'{save_name} will be saved to {outdir}.')

test_types = ['VISUAL_INSPECTION_RECEPTION', 
              'NO_PPA', 
              'PEDESTAL_TRIM_PPA',
              'STROBE_DELAY_PPA',
              'RESPONSE_CURVE_PPA',
              'NO_BURNIN',
              'PEDESTAL_TRIM_BURNIN',
              'STROBE_DELAY_BURNIN',
              'RESPONSE_CURVE_BURNIN'
              ]

def hybridsList(location):
    serialNumbers = []
    panels = dbCommands['listComponents'].run(project = 'S', componentType = 'HYBRID_TEST_PANEL')
    exclude_panels = ['20USET50000023','20USET50000047','20USET50000055','20USET00000048',
                      '20USET00000052','20USET00000055','20USET00000060','20USET40000041',
                      '20USET40000044','20USET40000047','20USET40000046','20USET40000048',
                      '20USET40000051','20USET50000085','20USET50000086','20USET50000087',
                      '20USET50000089','20USET50000002']
    for panel in tqdm(panels, total=len(panels), desc=f'Finding hybrids located at {location}'):
        if panel['currentLocation']['name']==location:
            panel_sn = panel['serialNumber']
            if panel_sn in exclude_panels: continue
            panel_details = dbCommands['getComponent'].run(component=panel_sn)
            for child in panel_details['children']: 
                if child['componentType']['code']=='HYBRID_ASSEMBLY' and child['component'] != None: serialNumbers.append(child['component']['serialNumber'])	
    print(f'Total number of hybrids: {len(serialNumbers)}')
    return serialNumbers


def getITSDAQVer(test_id):
    testRun = dbCommands['getTestRun'].run(testRun=test_id)
    if testRun['properties'] is None: return None
    for property in testRun['properties']:
        if property['code']=='system_info': 
            return property['value']['GIT describe']
        

def dbTestStatus(hyb_sn, location='University of Montreal'):
    stages = {test:{'passed':np.nan, 'itsdaq_ver':''} for test in test_types}
    testsRunList = dbCommands['getComponent'].run(component=hyb_sn)['tests']
    for t in testsRunList: 
        for run in t['testRuns']:
            if (t['code'] in stages) and (run['institution']['name']==location) and (stages[t['code']] != False): 
                stages[t['code']]['passed'] = run['passed'] # Specify whether pass/fail
                stages[t['code']]['itsdaq_ver'] = getITSDAQVer(run['id']) #Specify ITSDAQ version 
    return stages


def plotStatus(hybrids):
    hybrid_info = {}
    for hyb_run in tqdm(hybrids, total=len(hybrids), desc='Obtaining hybrid test data: '):
        hyb = hyb_run.split('_')[0]
        hybrid_info[hyb_run] = dbTestStatus(hyb)

    col_labels = ['VI_R','NO','PT','SD','RC','NO_BI','PT_BI','SD_BI','RC_BI']

    # Prepare the figure and axis
    fig, ax = plt.subplots()

    # Loop through hybrids and tests to draw colored rectangles
    for i, hybrid in enumerate(hybrids):
        for j, test in enumerate(test_types):
            test_data = hybrid_info[hybrid].get(test)
            
            # Default empty (no data)
            color = 'white'
            
            if test_data is not None:
                passed = test_data['passed']
                itsdaq_ver = test_data['itsdaq_ver']
                
                # Color based on pass/fail
                if not np.isnan(passed): color = 'green' if passed else 'red'
                rect = plt.Rectangle((i, j), 1, 1, facecolor=color, edgecolor='black')
                ax.add_patch(rect)
                
                # Apply hatching if old itsdaq_ver 
                if itsdaq_ver != 'itsdaq_23_3_2023-1445-g1d4607edb' and test != 'VISUAL_INSPECTION_RECEPTION' and (not np.isnan(passed)): rect.set_hatch('//')
            
            # Draw an empty box if no test data
            else:
                rect = plt.Rectangle((i, j), 1, 1, facecolor='white', edgecolor='black')
                ax.add_patch(rect)

    # Set the x-axis labels (hybrid serial numbers) and y-axis labels (test names)
    ax.set_xticks(np.arange(len(hybrids))+0.5)
    ax.set_xticklabels(hybrids, rotation=-45, ha='left')
    ax.set_yticks(np.arange(len(test_types)) + 0.5)
    ax.set_yticklabels(col_labels)

    # Set limits, aspect ratio, and grid formatting
    ax.set_xlim(0, len(hybrids))
    ax.set_ylim(0, len(test_types))
    ax.invert_yaxis()  # To have test1 at the top
    ax.set_aspect('auto')

    # Add grid lines to separate the boxes clearly
    ax.grid(False)  # Seaborn will handle grid lines for aesthetic clarity
    
    plt.gcf().set_figwidth(2 * (len(hybrids) / 32.) * plt.gcf().get_figwidth())
    plt.tight_layout()
    plt.savefig(f'{outdir}/{save_name}', dpi=300)
    plt.close()

if __name__ == '__main__':
    with open('processed_burnin_defects_RESPONSE_CURVE.json', 'r') as file: data = json.load(file)
    hybrids = [sn for t in data.keys() for sn in sorted(data[t].keys(), key=lambda sn: int(sn[7:]))]
    plotStatus(hybrids)
