# Montreal Burn-In Analysis

## Description


`burninResults.py` extracts all relevant data from the *.json files generated during hybrid burn-ins at the Montreal MIL Burn-In site. Once this data is extracted and organized, various plots are created and stored for analysis. 

`check_burnin_results_db.py` retrieves and saves a plot containing the *pass/fail* information from the database for the panels analyzed by `burninResults.py`.

## Usage
There are two input variables needed when using `burninResults.py`:

```
python3 burninResults.py <burn-in-data-directory> <plots-output-directory>
```

There is one variable needed when using `check_burnin_results_db.py`:

```
python3 check_burnin_results_db.py <output-directory>
```
`check_burnin_results_db.py` must only be used **after** `burninResults.py` has been run.


### ITk Database Access

If `ABCFuse_sn_map.json` has not been created, the ITk Database will have to be accessed. To grant this access, one can either provide a token in the terminal assigned to the variable `ITK_DB_AUTH` before running the script, or when prompted, provide two access codes associated with their PLUS4U account. For the former option, a token can be obtained [here](https://uuidentity.plus4u.net/uu-identitymanagement-maing01/a9b105aff2744771be4daa8361954677/showToken) and it can used as follows:

```
export ITK_DB_AUTH=eyJraWQiOiJhZWZkYzEyMjczMmY0N2E5ODVmOWQ3YzJhMWY0MjQ2Zjc4NGVlNzVhNjZjMjQwNmM5YWQwMjRjOTE2M2JhOTgiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9. eyJzdWIiOiI2Mjc5ZDgwMTA4OGIzZjE5NDdlZDQxMzAiLCJ1dWlkZW50aXR5IjoiNjA1Mi04MDU3LTEiLCJjbGllbnRfdXJpIjoidXJuOnV1b2lkY2NsaWVudDp1dS1pZGVudGl0eW1hbmFnZW1lbnQtbWFpbmcwMVwvNmQ3YzMxN2FmYzk5NDQ1NWIyOTQ4YTk3NjM0NjJiZWUtYTliMTA1YWZmMjc0NDc3MWJlNGRhYTgzNjE5NTQ2NzciLCJhdXRoX2lkIjoiT2NKX182T2lDTjgtU0VDeWppTGlBem5pNDU4OVA4cnh6Qll3TU92UkYxcyIsImNsaWVudF91dWFwcGtleSI6InV1LWlkZW50aXR5bWFuYWdlbWVudC1tYWluZzAxXC82ZDdjMzE3YWZjOTk0NDU1YjI5NDhhOTc2MzQ2MmJlZS1hOWIxMDVhZmYyNzQ0NzcxYmU0ZGFhODM2MTk1NDY3NyIsImNsaWVudF9hY3IiOiJsb3ciLCJpc3MiOiJodHRwczpcL1wvdXVpZGVudGl0eS5wbHVzNHUubmV0XC91dS1vaWRjLW1haW5nMDJcL2JiOTc3YTk5ZjRjYzRjMzdhMmFmY2UzZmQ1OTlkMGE3XC9vaWRjIiwiY2xpZW50X3V1aWRlbnRpdHlfdHlwZSI6InV1QXdpZEVlIiwibm9uY2UiOiJzOW1wbzFiaXJzbyIsImNsaWVudF91dWlkZW50aXR5IjoiMzE5Ni0zMjg3LTE3OTAtMSIsImFjciI6InN0YW5kYXJkIiwiYXVkIjpbImE5YjEwNWFmZjI3NDQ3NzFiZTRkYWE4MzYxOTU0Njc3IiwiaHR0cHM6XC9cLyIsImh0dHA6XC9cL2xvY2FsaG9zdCJdLCJjbGllbnRfdXVpZGVudGl0eV9uYW1lIjoiQVdJRCBhOWIxMDVhZmYyNzQ0NzcxYmU0ZGFhODM2MTk1NDY3NyIsIm5iZiI6MTY1NzM5OTQ4OSwidXVpZGVudGl0eV90eXBlIjoidXVQZXJzb24iLCJhdXRoX3RpbWUiOjE2NTczOTY1NzQsIm5hbWUiOiJXZW4gQ2hhbyBDaGVuIiwiZXhwIjoxNjU3NDAxMjg5LCJpYXQiOjE2NTczOTk0ODksImp0aSI6Ijg0MTViODVmNzIzOTRlZGM4NTJhNDVlZjA0OGIzMDQ2In0.LuoPo1cyC28XacTB7ow7wYVgtG0LVVO4jjcbHB8HUdF5o5du_YpOckBtjMk6bjQZAtBKW_H6HFIwmpSvdYsDv6a3JnkNq1PRXrEOtAZO9s4UBjb1KizwOnz3tduZ9pBT-NK99dURutaIjeoIps1lr0z3otyk84y-2LwsnfKEdD11ZHVsFy7RIuB77vSIqPDjbbCFtmR_SwNpoDGxa57nBG9pDPjfhDSh6GLaLljMcmXtS2UYNt64CujyhIsMImQM9WVWcwAJaUN7xb7Tdm7o_Jd_THiRpJq-LNNKiyLpTml7XSbCX7YrbcuMcfpfYBKabw7WtnFHqZE7SEXUcZ3nqA
```

## Outputs

Four *.json files and (at most) three types of plots will be created. The files are described below.

#### burnin_results_*.json
```
{..., hybrid serial number: [names of all *.json files generated for hybrid during burn-in], ...}
```

#### burnin_defects_results_*.json
```
{..., hybrid serial number: {..., property_i: [..., [chip_j values for each cycle], ...], ..., 'defects': [..., {..., channel_k in cycle_n: defect type, ...}, ...], 'time_stamps': [time that each cycle occured in hours], 'host': 'machine'}, ...}
```

#### processed_burnin_defects_*.json
```
{..., hybrid type: {..., hybrid serial number: {..., property_i_means: [mean values across burn-in for each chip], property_i_stds: [stds across burn-in for each chip], ..., 'summed_defects': {..., channel_k: number of defects across burn-in, ...}, 'n_cycles': number of cycles across burn-in, 'n_failed_channels': number of failed channels}, ...}, ...}
```
#### ABCFuse_sn_map.json
```
{ABCFuseID: hybrid serial number}
```

A list of panels missing PEDESTAL_TRIM data will also be printed. 

Not all three plots are created for each test. The table below shows which plots will be produced.

| Test | Property Average v. Cycle | Failed Channels v. Hybrid | Property Average & SD v. Hybrid |
| ---- | ------------------------- | ------------------------- | ------------------------------- |
| RC | Y | Y | Y |
| SD | Y | N | N |
| NO | Y | N | N |
| PT | N | Y | N |

