"""
Organizes, plots, and analyzes burn-in data. 
Author: Ethan Meszaros

Example usage:
$ python3 burninResults.py burnindata plots
"""

import os
import os.path as op
import sys
import pathlib
import json
import fnmatch
from datetime import datetime
import matplotlib.ticker as tick
from itertools import zip_longest
import numpy as np
from matplotlib import pyplot as plt
from tqdm import tqdm 

# itkdb package is not working -> this will suffice
from databaseUtilitiesEthan import commands as dbCommands

test = 'RESPONSE_CURVE'  # RESPONSE_CURVE, NO, STROBE_DELAY, PEDESTAL_TRIM
PG = 3  # 3 or 10
reject_outliers = True

if len(sys.argv) > 1:
    in_path = sys.argv[1] # directory containing burn-in *.json files
    print(f"Location of burn-in data: {in_path}")
else:
    print("Warning: No burn-in data directory supplied")
    exit()

out_path = os.getcwd()  # directory where *_results_*.json files are saved
save_path = os.getcwd()+f'/{sys.argv[2]}/'+test  # directory where plots are saved
save_defect_path = os.getcwd()+f'/{sys.argv[2]}/'+test+'/DEFECTS/'  # directory where defect plots are saved


def load_json(path):
    'Adapted from Matthew Basso'
    with open(path, 'r') as f:
        return json.load(f)
    return


def save_json(data, path):
    'Adapted from Matthew Basso'
    with open(path, 'w') as f:
        json.dump(data, f, indent=4)
    return


def get_parent_sn(sn, typ='HYBRID_ASSEMBLY'):
    ''' str -> str
    Accesses database and finds parent of given component with serial number 'sn'.
    Returns parent serial number. 
    '''
    c = dbCommands['getComponent'].run(component=sn)
    for p in c['parents']:
        if p['componentType']['code'] == typ and p['component'] != None:
            SN = p['component']['serialNumber']
            # return first result
            return SN


def get_sn_from_ABC(map={}, file=None, in_path=None, ABC_fuse=None):
    '''
    Takes *.json file from burn-in OR ABC FuseID and returns corresponding hybrid 
    serial number. Adapted from Matthew Basso.
    '''
    if ((file, in_path) != (None, None) and
        pathlib.Path(file).suffix == '.json' and
        ABC_fuse == None):
        j = load_json(op.join(in_path, file))
        for c in j['properties']['det_info']['ABC_Fuse']:
            if c != None:
                # use first non-empty ABC ID
                ABC = c
                break
    elif ABC_fuse != None and (file, in_path) == (None, None):
        ABC = ABC_fuse
    else:
        raise ValueError("Incorrect inputs or file format.")
    if map.get(ABC_fuse): return map[ABC_fuse]
    component = dbCommands['listComponentsByProperty'].run(project='S',
                                                           componentType='ABC',
                                                           propertyFilter=[{'code': 'ID',
                                                                            'operator': '=',
                                                                            'value': 'ABC'+ABC[2:]
                                                                            }
                                                                           ]
                                                           )
    ABC_sn = component[0]['serialNumber']
    hyb_sn = get_parent_sn(ABC_sn)
    if hyb_sn == None:
        print(f"No serial number was found for {file}.")
    else:
        map[ABC_fuse] = hyb_sn
        return hyb_sn


def filename_properties(f):
    '''str -> str, str, str
    Extracts portions of a *.json filename, such as hybrid type (e.g. R5), hybrid position, 
    serial number, and date of burn-in.
    '''
    typ, hyb, sn, date = f.split('_')[1][0:2], f.split('_')[2], f.split('_')[5], f.split('_')[6]
    return typ, hyb, sn, date


def check_chip_number(burnin_results_json, load_message='Checking chip numbers'):
    '''dict -> None
    Takes as input the dictionary of hybrid serial numbers (keys) and *.json files (values), 
    loops through them to check the chip number of each *.json file, and saves a file containing 
    the *.json files that have incorrect chip numbers. 
    '''
    defective_hybrids = {}

    for hybrid_sn in tqdm(burnin_results_json, total=len(burnin_results_json.keys()), unit='hybrid', desc=load_message): 

        defective_hybrids[hybrid_sn] = {'files':[]}
        YY = hybrid_sn[5:7]
        chip_num_ref = _CHANNELS_PER_HYBRID[_YY_TO_TYPE[YY]] / 256

        for file in burnin_results_json[hybrid_sn]:
            j = load_json(op.join(in_path, file))
            chip_num = len(j['properties']['det_info']['Channel'])
            if chip_num != chip_num_ref: defective_hybrids[hybrid_sn]['files'].append(file)

        num_files = len(burnin_results_json[hybrid_sn])
        fail_per = len(defective_hybrids[hybrid_sn]['files'])/num_files
        defective_hybrids[hybrid_sn]['fail_per'] = round(fail_per, 1)

    print('Saving '+'chip_num_check.json')
    save_json(defective_hybrids, op.join(out_path, 'chip_num_check.json'))


def extract_json_files(base_dir, conditions, exclude_dirs=None):
    json_paths = []
    found = []
    exclude_dirs = exclude_dirs or []

    # To calculate total number of directories, including subdirectories
    def count_dirs(path):
        total = 0
        with os.scandir(path) as it:
            for entry in it:
                if entry.is_dir() and entry.name not in exclude_dirs:
                    total += 1
                    total += count_dirs(entry.path)  # Recursively count subdirectories
        return total
    
    total_dirs = count_dirs(base_dir) + 1  # +1 for the base directory itself

    # Recursive function to scan directories
    def scan_directory(path):
        with os.scandir(path) as entries:
            for entry in entries:
                if entry.is_dir() and entry.name not in exclude_dirs:
                    # Recurse into subdirectories
                    scan_directory(entry.path)
                elif entry.is_file() and any(direc not in entry.path for direc in exclude_dirs):
                    # Check each file against the conditions
                    for condition in conditions:
                        if fnmatch.fnmatch(entry.name, condition):
                            json_paths.append(entry.path)
                            found.append(condition)

    scan_directory(base_dir)
    return json_paths 


def panelList(l):
	serialNumbers = []
	components = dbCommands['listComponents'].run(project = 'S', componentType = 'HYBRID_TEST_PANEL')
	for c in components:
		if c['currentLocation']['name']==l: serialNumbers.append(c['serialNumber'])	
	return serialNumbers


def hybridsList(location):
    serialNumbers = []
    panels = dbCommands['listComponents'].run(project = 'S', componentType = 'HYBRID_TEST_PANEL')
    exclude_panels = []
    for panel in tqdm(panels, total=len(panels), desc=f'Finding hybrids located at {location}'):
        if panel['currentLocation']['name']==location:
            panel_sn = panel['serialNumber']
            if panel_sn in exclude_panels: continue
            panel_details = dbCommands['getComponent'].run(component=panel_sn)
            for child in panel_details['children']: 
                if child['componentType']['code']=='HYBRID_ASSEMBLY' and child['component'] != None: serialNumbers.append(child['component']['serialNumber'])	
    print(f'Total number of hybrids: {len(serialNumbers)}')
    return serialNumbers


def key(fn):
    '''str -> int, int
    Extracts run and burst number from *.json file name, which are used to organize 
    the list of files. Adapted from Matthew Basso.
    '''
    hyb, date, run, burst = fn.rstrip('_'+test+'_PPA.json').split('_')[-4:]
    return int(run), int(burst), hyb, int(date)


def collect_test_data_files(in_path, out_path, test, PG, save_name, load_message='Collecting data files'):
    '''str, str, str, int, str -> dict
    Loops through burn-in files and creates dict where keys are hybrid serial numbers
    and values are lists of files corresponding to that hybrid. Accesses database 
    to find hybrid serial number corresponding to the ABC FuseID contained within 
    each datafile. 

    Files saved: "burnin_results_*.json" & "ABCFuse_sn_map.json"

    Adapted from Matthew Basso. 
    '''
    in_path, out_path = op.join(in_path, ''), op.join(out_path, '') 
    map = load_json(op.join(out_path, '')+'ABCFuse_sn_map.json') if op.isfile(op.join(out_path, 'ABCFuse_sn_map.json')) else {}

    # Compile serial numbers of hybrids currently on-site
    #hybrids_onsite = hybridsList('University of Montreal')

    # Extract json files satisfying certain conditions
    conditions = [f'*{test}*_PPA.json'] 
    files = extract_json_files(in_path, conditions, exclude_dirs=['bad_files', 'badfiles'])

    dFiles = {}
    #hybrids_found = [] # list of on-site hybrids with found data
    for f in tqdm(files, total=len(files), unit='file', desc=load_message):
        j = load_json(f)
        try:
            j['properties']['det_info']['ABC_Fuse']
        except KeyError:
            present = False
        else:
            present = True
        if present:
            # pick ABC ID for database to reference
            for c in j['properties']['det_info']['ABC_Fuse']:
                if c != None and len(c)==8:
                    ABC = c
                    break
        else:
            continue
        r, b, h, d = key(f)
        if test == 'RESPONSE_CURVE':
            pointGain = len(j['properties']['scan_info']['points'])
            if pointGain != PG: continue
        try:
            sn = get_sn_from_ABC(map, ABC_fuse=ABC)
        except (KeyError, IndexError):
            print(f'No hybrid linked to ABC Fuse ID for {f}.')
            continue
        # add run number at the end of the serial number
        if (sn is not None): #and (sn in hybrids_onsite):
            #if sn not in hybrids_found: hybrids_found.append(sn)
            sn += '_'+str(r)
        else:
            continue
        if not sn in dFiles.keys(): dFiles[sn] = []
        dFiles[sn].append(op.relpath(f, in_path))
    for sn in list(dFiles):
        dFiles[sn] = sorted(dFiles[sn], key=key)
        # hybrids with less than 100 data files are rejected
        if len(dFiles[sn]) < 70: dFiles.pop(sn)

    #print(f'The following hybrids were not found: {sorted(set(hybrids_onsite) - set(hybrids_found))}')
    print('Saving '+save_name)

    # map is saved as a *.json file to prevent future database access
    save_json(map, op.join(out_path, '')+'ABCFuse_sn_map.json')
    save_json(dFiles, op.join(out_path, '')+save_name)
    return dFiles


def time_stamp(file, in_path):
    '''str, str -> str 
    Takes burn-in *.json file and returns time (in hours) that the file was 
    generated.
    '''
    t = load_json(op.join(in_path, file))
    return datetime.strptime((t['properties']['system_info']['Analysis_time'][0:10] +
                              ' ' +
                              t['properties']['system_info']['Analysis_time'][11:19]),
                             "%Y-%m-%d %H:%M:%S"
                             )


keys = {'RESPONSE_CURVE': ['gain_away',
                           'gain_under',
                           'vt50_away',
                           'vt50_under',
                           'innse_away',
                           'innse_under',
                           'outnse_away',
                           'outnse_under'
                           ],
        'STROBE_DELAY': ['StrobeDelay_away',
                         'StrobeDelay_under'
                         ],
        'NO': ['enc_est_away',
               'enc_est_under'
               ],
        'PEDESTAL_TRIM': ['trim_away',
                          'trim_under'
                          ]
        }

def filter_none(values): 
    if type(values) in [float, int]: 
        return values
    else:
        return [v for v in values if v is not None]
    
def transpose_non_hom(matrix):
    transposed = list(zip_longest(*matrix, fillvalue=None))
    # Remove the `None` values from the transposed matrix
    transposed = [[item for item in row if item is not None] for row in transposed]
    return transposed

def collect_burn_in_data(dFiles, in_path, out_path, test, save_name, keys=keys[test], load_message='Collecting data'):
    '''dict, str, str, str, str -> dict
    Creates a dict where keys are hybrid serial numbers and values are lists of 
    average property values for each chip, channels containing defects, and the time
    stamp of each *.json file across an entire burn-in. 

    File saved: "burnin_defects_results_*.json"

    Adapted from Matthew Basso.
    '''
    data = {}
    for sn, files in tqdm(dFiles.items(), total=len(dFiles.items()), unit='file', desc=load_message):
        data[sn] = {k: [] for k in keys}
        data[sn]['defects'] = []
        data[sn]['time_stamps'] = []
        last_burst = None
        for fn in files:
            burst = fn.rstrip('_'+test+'_PPA.json').split('_')[-1]
            j = load_json(op.join(in_path, fn))
            # add property averages for each chip across entire burn-in
            for k in keys:
                means = []
                results = j['results'][k]
                for c in results: means.append(np.mean(filter_none(c)))
                # data structure is different for each test 
                if test in ['NO', 'STROBE_DELAY']: means = list(filter(lambda a: a != -1, results))
                if test == 'PEDESTAL_TRIM': means = np.mean(list(filter(lambda a: a != -1, results)))
                # control for files with the same run number 
                if burst == last_burst:
                    data[sn][k][-1] += means
                else:
                    data[sn][k].append(means)
                pass
            # create time stamp data
            if last_burst == None:
                # find starting time
                s = time_stamp(fn, in_path)
            elif burst == last_burst:
                pass
            else:
                t = time_stamp(fn, in_path) - s
                data[sn]['time_stamps'].append(t.total_seconds()/3600)
            data[sn]['host'] = j['properties']['system_info']['host']
            data[sn]['n_channels'] = len(j['properties']['det_info']['Channel']) * 256
            # list channels with defects for each cycle (only RC and PT)
            if test in ['RESPONSE_CURVE', 'PEDESTAL_TRIM']:
                defects = {}
                for d in j.get('defects', []):

                    defect_properties = d['properties']
                    fch = defect_properties.get('channel_from', -1)
                    lch = defect_properties.get('channel_to', -1)
                    ch = defect_properties.get("channel", -1)
                    stream = defect_properties['chip_bank']

                    chip = defect_properties.get('chip_in_histo', -1)

                    if ch != -1:
                        # Single channel
                        channels = [ch]
                    elif fch != -1 and lch != -1:
                        # Range of channels
                        channels = range(fch, lch + 1)
                    elif chip != -1:
                        # Whole chip
                        channels = range(chip * 128, (chip+1) * 128)
                    else:
                        print("Odd Json.  Has neither channel_from and channel_to or channel fields in defects/properties")
                        continue
                    
                    for c in channels: 
                        # handle other "hybrid" by offsetting channel number
                        if burst == last_burst: c += (6 * 256)

                        ch_stream = f"{c} {stream}"
                        if (ch_stream not in defects.keys()): defects[ch_stream] = d['name']
                    pass
                if burst == last_burst:
                    #assert not (set(data[sn]['defects'][-1].keys()) & set(defects.keys()))
                    data[sn]['defects'][-1].update(defects)
                else:
                    data[sn]['defects'].append(defects)
            last_burst = burst
        pass
    # transpose array for each k to organize by chip
    if test != 'PEDESTAL_TRIM':
        for sn in list(dFiles):
            for k in keys:
                ar = transpose_non_hom(data[sn][k])
                data[sn][k] = ar
    print('Saving '+save_name)
    save_json(data, op.join(out_path, '')+save_name)
    return data


_YY_TO_TYPE = {
    'H0': 'R0H0',
    'H1': 'R0H1',
    'H2': 'R1H0',
    'H3': 'R1H1',
    'H4': 'R2H0',
    'H5': 'R3H0',
    'H6': 'R3H1',
    'H7': 'R3H2',
    'H8': 'R3H3',
    'H9': 'R4H0',
    'HA': 'R4H1',
    'HB': 'R5H0',
    'HC': 'R5H1'
}


def get_type(sn):
    '''Adapted from Matthew Basso'''
    return _YY_TO_TYPE.get(sn[5:7])


def process_burn_in_defects(data, save_name, keys=keys[test], load_message='Processing defects'):
    '''dict, str -> dict
    Creates dict where hybrid types (e.g. R0H1) and serial numbers are keys and
    values are means and SDs across entire burn-in run, number of defects found
    for a given channel across entire burn-in run, number of failed channels 
    (criteria: channel exhibits defects for more than 15% of cycles), and number
    of cycles. 
    
    File saved: 'processed_burnin_defects_*.json'
    
    Adapted from Matthew Basso.'''
    processed_data = {t: {} for t in _YY_TO_TYPE.values()}
    for sn in tqdm(data.keys(), total=len(data.keys()), unit='file', desc=load_message):
        print("On: "+sn)
        t = get_type(sn)
        processed_data[t][sn] = {}
        for k in data[sn].keys():
            if k == 'defects':
                summed_defects = {}
                for d in data[sn][k]:
                    for c in d.keys():
                        # count the amount of defects for a given channel 
                        if c not in summed_defects.keys(): summed_defects[c] = 0
                        summed_defects[c] += 1
                    pass
                n_cycles = len(data[sn]['time_stamps'])
                n_channels = data[sn]['n_channels']
                n_failed_channels = 0
                for v in summed_defects.values():
                    if v > 0.15 * n_cycles: n_failed_channels += 1
                processed_data[t][sn].update({'summed_defects': summed_defects,
                                              'n_cycles': n_cycles,
                                              'n_channels': n_channels,
                                              'n_failed_channels': n_failed_channels})
            # find means and SDs
            elif k in keys:
                chip_means = []
                chip_stds = []
                for c in data[sn][k]:
                    chip_means.append(np.mean(c))
                    chip_stds.append(np.std(c))
                processed_data[t][sn][k + '_chip_means'] = chip_means
                processed_data[t][sn][k + '_chip_stds'] = chip_stds
            pass
        pass
    print('Saving '+save_name)
    save_json(processed_data, op.join(out_path, '')+save_name)
    return processed_data

def outliers(data, remove):
    if remove: 
        return [v if abs(v) <= 1000 else None for v in data]
    else:
        return data

def y_avg(d, sn, k, ax):
    '''dict, str, str, axis -> list
    Calculates the average of a property across all chips of a hybrid for a 
    given cycle and plots these averages across an entire burn-in run. 
    '''
    y_avg = []
    d_transpose = transpose_non_hom(d[sn][k])
    n_cycles = len(d_transpose)
    for f in d_transpose: y_avg.append(np.average(filter_none(outliers(f, reject_outliers)))) if len(f) else y_avg.append(None)
    ax.plot(range(n_cycles),
            y_avg,
            '-o',
            color='k',
            label='All')
    return y_avg

def plot(d, sn, PG, save_path, keys=keys[test]):
    '''dict, str, int, str -> None
    Plots property values for each chip on a given hybrid across a burn-in run. 
    '''
    t, h = d[sn]['time_stamps'], d[sn]['host']
    
    def get_pc(hyb):
        for pc, hybs in host.items():
            if hyb in hybs:
                return pc

    if h == '':
        # use 'machine_hybrid_map.json' to find host of hybrid 
        if op.isfile('machine_hybrid_map.json'):
            host = load_json(op.join(out_path, 'machine_hybrid_map.json'))
            h = get_pc(sn.split('_')[0])
    for k in keys:
        fig, ax1 = plt.subplots(1,
                                1,
                                figsize=(12, 6),
                                layout='constrained'
                                )
        test_name = k.replace('_', ' ').upper()
        #panel_sn = get_parent_sn(sn)
        title = f'Average {test_name} per Cycle for {sn} ({str(PG)}PG) ({h})'
        #subtitle = f'Panel: {panel_sn}'
        fig.suptitle(title)
        #plt.title(subtitle, style="italic", color='gray')
        for chip, chip_data in enumerate(d[sn][k]):
            y = outliers(chip_data, reject_outliers)
            x = range(len(y))
            ax1.plot(x,
                     y,
                     '-o',
                     label=f'Chip {chip}'
                     )
            
            #ax1.set_title(title)
            ax1.set_xlabel('Cycle')
            ax1.set_ylabel('Average '+test_name)
            ax1.set_xlim([0, len(chip_data)])
            def time_ax(x, pos):
                if x < len(t):
                    return int(t[int(x)])
                else:
                    return None
        # create time axis
        ax2 = ax1.twiny()
        ax2.set_xlabel('Time (h)')
        ax2.set_xlim(ax1.get_xlim())
        ax2.xaxis.set_major_formatter(tick.FuncFormatter(time_ax))
        ax2.locator_params(axis='x', tight=True, nbins=10)
        y_avg(d, sn, k, ax1)
        ax1.legend(bbox_to_anchor=(1.0, 1.0), loc='upper left')
        # save *.png figures
        fig.savefig(op.join(save_path, sn+'_'+k.upper()+'.png'))
        plt.clf()
        plt.close()


_CHANNELS_PER_HYBRID = {
    'R0H0':  8 * 256,
    'R0H1':  9 * 256,
    'R1H0': 10 * 256,
    'R1H1': 11 * 256,
    'R2H0': 12 * 256,
    'R3H0':  7 * 256,
    'R3H1':  7 * 256,
    'R3H2':  7 * 256,
    'R3H3':  7 * 256,
    'R4H0':  8 * 256,
    'R4H1':  8 * 256,
    'R5H0':  9 * 256,
    'R5H1':  9 * 256,
}


def plot_defects(data, save_path, save_name, title=None, types=None, fraction=True, formats=['png']):
    '''dict, str, str -> None
    Plots the fraction of defective channels (out of total channels) for each hybrid. 
    
    Adapted from Matthew Basso.
    '''
    types = [key for key in data.keys() if len(data[key]) != 0]
    xticklabels = []
    weights = []
    for t in types:
        for sn in sorted(data[t].keys(), key=lambda sn: int(sn[7:])):
            xticklabels.append(sn)
            weights.append(data[t][sn]['n_failed_channels'] /
                           (data[t][sn]['n_channels'] / 100. if fraction else 1.))
        pass  
    if not weights:
        #print('WARNING: no data present, returning!')
        return
    bins = np.arange(len(xticklabels) + 1)
    plt.gcf().set_figwidth(2 * (len(xticklabels) / 32.) * plt.gcf().get_figwidth())
    plt.hist(bins[:-1], bins, weights=weights, histtype='stepfilled', stacked=True, color='grey')
    for xx in bins[1:-1]:
        plt.axvline(x=xx, color='black', linestyle=':', alpha=0.7)
    plt.gca().set_xticks(bins[:-1] + 0.5, xticklabels, rotation=-45, ha='left', va='top')
    if not fraction: plt.gca().yaxis.set_major_locator(tick.MaxNLocator(integer=True))
    plt.xlim((0, len(xticklabels)))
    #plt.ylim((0, 1.1))
    if title: plt.title(title)
    plt.xlabel('Serial number')
    plt.ylabel('Fraction of channels [%]' if fraction else 'Number of channels')
    plt.axhline(y=1., color='red', linestyle='--')
    for f in formats:
        path, _ = op.splitext(op.join(save_path, save_name))
        path += ('.' + f)
        plt.savefig(path, bbox_inches='tight')
    plt.close()


_COLOR = {
    0: 'brown',
    1: 'darkorange',
    2: 'gold',
    3: 'olivedrab',
    4: 'aquamarine',
    5: 'gray',
    6: 'cadetblue',
    7: 'skyblue',
    8: 'red',
    9: 'palegreen',
    10: 'orchid',
    11: 'thistle',
    12: 'green'
}


def plot_std_mean_per_hybrid(data, t, savepath, keys=keys[test], title=None, formats=['pdf', 'png']):
    if not data[t]: return
    print(t)
    n_chips = int(_CHANNELS_PER_HYBRID[t] / 256)
    for k in keys:
        xticklabels = []
        weights_mean = [[] for _ in range(n_chips)]
        weights_std = [[] for _ in range(n_chips)]
        f, (ax1, ax2) = plt.subplots(2,
                                     1,
                                     sharex=True,
                                     )
        if title: f.suptitle(title)
        plt.gcf().set_figheight(7)
        try:
            for c in range(n_chips):
                for sn in sorted(data[t].keys(), key=lambda sn: int(sn[7:])):
                    if sn not in xticklabels: xticklabels.append(sn)
                    try:
                        weights_mean[c].append(data[t][sn][k + '_chip_means'][c])
                        weights_std[c].append(data[t][sn][k + '_chip_stds'][c])
                    except IndexError:
                        weights_mean[c].append(0)
                        weights_std[c].append(0)
                bins = np.arange(len(xticklabels) + 1)
                prop = k.split('_')[0].capitalize()+' '+k.split('_')[-1].capitalize()
                for xx in bins[1:-1]:
                    ax1.axvline(x=xx, color='black', linestyle=':', alpha=0.7)
                    ax2.axvline(x=xx, color='black', linestyle=':', alpha=0.7)
                ax1.hist(bins[:-1],
                         bins,
                         weights=weights_mean[c],
                         histtype='step',
                         color=_COLOR[c],
                         linewidth=1.5
                         )
                ax1.hist(bins[:-1],
                         bins,
                         weights=weights_mean[c],
                         histtype='stepfilled',
                         alpha=0.4,
                         label='Chip '+str(c),
                         color=_COLOR[c]
                         )
                ax1.set_ylabel('Average ' + prop)
                ax1.set_xlim((0, len(xticklabels)))
                #print('WEIGHTS MEAN: ', weights_mean[c])
                #m = np.nanmean([w for w in weights_mean[c] if w != 0])
                ax2.hist(bins[:-1],
                         bins,
                         weights=weights_std[c],
                         histtype='step',
                         color=_COLOR[c],
                         linewidth=1.5
                         )
                ax2.hist(bins[:-1],
                         bins,
                         weights=weights_std[c],
                         histtype='stepfilled',
                         alpha=0.4,
                         color=_COLOR[c]
                         )
                plt.gca().set_xticks(bins[:-1] + 0.5,
                                     xticklabels,
                                     rotation=-45,
                                     ha='left',
                                     va='top'
                                     )
                ax2.set_xlabel('Serial Number')
                ax2.set_ylabel('STD of ' + prop)
            m = np.nanmean([w for c in weights_mean for w in c])
            ax1.set_ylim(bottom=m-0.1*m)
        except ValueError:
            plt.close()
            return
        plt.figlegend(bbox_to_anchor=(0.9, 0.6), loc='upper left')
        for fmt in formats:
            savename = k.replace('_mean_', '_') + \
                '_mean_std_from_burn_in_%s.pdf' % t
            path, _ = op.splitext(op.join(savepath, savename))
            path += ('.' + fmt)
            f.savefig(path, bbox_inches='tight')
        plt.clf()
        plt.close()


def missing_pedestal_data():
    rcPath = op.join(out_path, 'burnin_results_RESPONSE_CURVE.json')
    ptPath = op.join(out_path, 'burnin_results_PEDESTAL_TRIM.json')
    if op.isfile(rcPath) and op.isfile(ptPath):
        rc, pt = list(load_json(rcPath).keys()), list(load_json(ptPath).keys())
        print('Hybrids that have no pedestal trim data:')
        hybrids = [sn for sn in rc if sn not in pt]
        print(hybrids)
    else:
        print('Both RESPONSE_CURVE and PEDESTAL_TRIM files must be created '
              'before missing PEDESTAL_TRIM data can be determined.')

def main():
    if op.isdir(in_path) and op.isdir(out_path):
        burnin_results = collect_test_data_files(in_path,
                                                 out_path, 
                                                 test,  
                                                 PG,
                                                 'burnin_results_'+test+'.json')
        check_chip_number(burnin_results)
        burnin_defects_results = collect_burn_in_data(burnin_results,
                                                      in_path,
                                                      out_path,
                                                      test,
                                                      'burnin_defects_results_'+test+'.json')
        if test != 'PEDESTAL_TRIM':
            print('Plotting data.')
            if not op.isdir(save_path): os.makedirs(save_path)
            for sn in burnin_defects_results.keys():
                print(sn)
                plot(burnin_defects_results,
                     sn,
                     PG,
                     save_path
                     )
        if test in ['NO', 'STROBE_DELAY']: return
        processsed_burnin_defects = process_burn_in_defects(burnin_defects_results,
                                                            'processed_burnin_defects_'+test+'.json')
        print('Plotting defects.')
        if not op.isdir(save_defect_path): os.makedirs(save_defect_path)
        if test in ['RESPONSE_CURVE', 'NO']:
            plot_std_mean_per_hybrid(processsed_burnin_defects,
                                    t,
                                    save_defect_path,
                                    title='Means & Standard Deviations Across Burn-in Cycles Per Hybrid : %s' % t)
        plot_defects(processsed_burnin_defects,
                     save_defect_path,
                     'defects_per_hybrid_from_burn_in_'+test+'.pdf',
                     types=list(processsed_burnin_defects.keys()),
                     title=test+' Defective Channels per Hybrid During Burn-in')
        if test == 'PEDESTAL_TRIM': missing_pedestal_data()
        print('Done!')
    else:
        print(f"One of the directories {in_path} or {out_path} does not exist.")


# executing commands
if __name__ == '__main__':
    main()
