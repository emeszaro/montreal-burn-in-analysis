#serialNumberList.py --- Script for creating "Hybrid_Info_original.txt" and "Hybrid_Info.txt", 
#which are lists of hybrid panels located at a given institute that have their ITSDAQ burn-in data 
#uploaded to ITkPD or are still under preparation.
#Created: 08/06/2023
#Author: Ethan Meszaros

from databaseUtilitiesEthan import commands as dbCommands

filename1 = 'Hybrid_Info_original_test.txt'
filename2 = 'Hybrid_Info_test.txt'

#define institute
l = 'University of Montreal'

#create list of panel serial numbers currently located at 'loc'
def panelList(l):
	serialNumbers = []
	components = dbCommands['listComponents'].run(project = 'S', componentType = 'HYBRID_TEST_PANEL')
	for c in components:
		if c['currentLocation']['name']==l: serialNumbers.append(c['serialNumber'])	
	return serialNumbers

#fill *.txt files with parent and child serial numbers, along with hybrid positions
def hybridInfo(fn1, fn2, l):
	print('Creating '+fn1+' and '+fn2)
	f1 = open(fn1, "w+")
	f1.write('#Panel SN, RFID, Hybrid SN, Hybrid Position, Stage\n#---------------------------------\n#Submitted\n')
	f2 = open(fn2, "w+")
	f2.write('#---------------------------------\n#Being Prepared\n')
	
	sList = panelList(l)

	for s in sList:
		p = dbCommands['getComponent'].run(component=s)	
		n1, n2 = (0,0)
		for c in p['children']:
			if (c['componentType']['code']=='HYBRID_ASSEMBLY' and 
			    c['component'] != None):
	
				s_child = c['component']['serialNumber']
				stage = dbCommands['getComponent'].run(component=s_child)['currentStage']['code']
				RFID = p['properties'][0]['value']
				pos = c['properties'][0]['value']
	
				if stage=='FINISHED_HYBRID':
					f1.write(s+', '+RFID+', '+s_child+', Hyb'+pos+', '+stage+'\n')
					n1+=1
				else:
					f2.write(s+', '+RFID+', '+s_child+', Hyb'+pos+', '+stage+'\n')
					n2+=1
	
		if n1 != 0 and n2 != 0:
			f1.write('\n\n')
			f2.write('\n\n')
		elif n1 != 0: f1.write('\n\n')
		elif n2 != 0: f2.write('\n\n')
	f1.close()
	f2.close()

	#append "Hybrid_Info.txt" to "Hybrid_Info_original.txt"
	f1 = open(fn1,'a+')
	f2 = open(fn2,'r')
	f1.write(f2.read())

	f1.close()
	f2.close()

hybridInfo(filename1, filename2, l)
print('Done')
